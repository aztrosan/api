//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparse = require('body-parser')
app.use(bodyparse.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var movimientosV2JSON = require('./movimientosv2.json');

var requestjson = require('request-json');

var urlClientesMLab = "https://api.mlab.com/api/1/databases/bestevez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientesMLab);



app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send('Hemos recibido su peticion get');
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res) {
  //res.send('Hemos recibido su peticion post')
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.put('/', function(req, res) {
  //res.send('Hemos recibido su peticion put')
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.delete('/', function(req, res) {
  //res.send('Hemos recibido su peticion delete')
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.get('/v1/Clientes/:idcliente', function(req, res) {
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})

app.get('/v1/movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos', function(req, res){
  res.json(movimientosV2JSON);
})

app.get('/v2/movimientos/:index', function(req, res) {
  console.log(req.params.index);
  res.send(movimientosV2JSON[req.params.index - 1]);
})

app.get('/v2/movimientosquery', function(req, res) {
  console.log(req.query);
  res.send('recibido');
})

app.post('/v2/movimientos', function(req, res){
  var nuevo = req.body
  nuevo.id = movimientosV2JSON.length + 1
  movimientosV2JSON.push(nuevo)
  res.send('Movimiento dado de alta');
})

app.put('/v2/movimientos', function(req, res){
  res.send('Hemos recibido su peticioń put de actualizacion de movimientos');
})

app.get('/v3/Clientes', function(req, res){
  var clienteMLab = requestjson.createClient(urlClientesMLab);
  clienteMLab.get ('',function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.post('/v3/Clientes', function(req, res){
  clienteMLab.post('',req.body,function(err,resM,body){
    res.send(body);
  })
})
